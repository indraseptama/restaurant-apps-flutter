import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class PromoEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class PageStarted extends PromoEvent {
  @override
  String toString() {
    return "PageStarted";
  }
}

class PromoSwiped extends PromoEvent {
  final int index;

  PromoSwiped({@required this.index});

  @override
  List<Object> get props => [index];

  @override
  String toString() {
    return "PromoSwiped at index: $index";
  }
}

class FavoriteButtonTapped extends PromoEvent {
  final bool isFavorite;
  final int index;
  FavoriteButtonTapped({@required this.isFavorite, @required this.index})
      : assert(isFavorite != null),
        assert(index != null);

  @override
  List<Object> get props => [isFavorite];

  @override
  String toString() {
    return super.toString();
  }
}
