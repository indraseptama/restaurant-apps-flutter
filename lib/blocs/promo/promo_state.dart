import 'package:awesomeapp/repositories/models/promo.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PromoState extends Equatable {
  @override
  List<Object> get props => [];
}

class CurrentPromoIndexChanged extends PromoState {
  final int currentIndex;

  CurrentPromoIndexChanged({@required this.currentIndex});

  @override
  List<Object> get props => [currentIndex];

  @override
  String toString() {
    return "CurrentPromoIndexChanged: $currentIndex";
  }
}

class CurrentPromoFavoriteButtonTapped extends PromoState {
  final bool isFavorite;

  CurrentPromoFavoriteButtonTapped({@required this.isFavorite});

  @override
  List<Object> get props => [isFavorite];

  @override
  String toString() {
    return "CurrentPromoFavoriteButtonTapped with promo index isFavorite: $isFavorite";
  }
}

class PromoLoading extends PromoState {
  @override
  String toString() {
    return 'PageLoading';
  }
}

class PromoLoaded extends PromoState {
  final List<Promo> promos;

  PromoLoaded({@required this.promos});

  @override
  List<Object> get props => [promos];

  @override
  String toString() {
    return "PromoLoaded with";
  }
}

class PromoFailedToDisplay extends PromoState {}
