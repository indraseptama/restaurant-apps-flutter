import 'package:awesomeapp/repositories/home_page_repository.dart';
import 'package:awesomeapp/repositories/models/promo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:awesomeapp/repositories/api/app_exceptions.dart';
import 'bloc.dart';

class PromoBloc extends Bloc<PromoEvent, PromoState> {
  HomePageRepository homePageRepository;
  int currentIndex = 0;
  List<Promo> promos;

  PromoBloc({this.homePageRepository}) : assert(homePageRepository != null);

  @override
  PromoState get initialState => PromoLoading();

  @override
  Stream<PromoState> mapEventToState(PromoEvent event) async* {
    if (event is PageStarted) {
      try {
        promos = await _getHomePagePromoData();
        yield PromoLoading();
        yield PromoLoaded(promos: this.promos);
      } on FetchDataException {
        yield PromoFailedToDisplay();
      } on BadRequestException {
        yield PromoFailedToDisplay();
      } on UnauthorisedException {
        yield PromoFailedToDisplay();
      } on InvalidInputException {
        yield PromoFailedToDisplay();
      }
    }
    if (event is PromoSwiped) {
      this.currentIndex = event.index;
      yield CurrentPromoIndexChanged(currentIndex: this.currentIndex);
    }
    if (event is FavoriteButtonTapped) {
      promos[event.index].isFavorite = !promos[event.index].isFavorite;
      yield CurrentPromoFavoriteButtonTapped(isFavorite: event.isFavorite);
    }
  }

  Future<List<Promo>> _getHomePagePromoData() async {
    List<Promo> data = homePageRepository.promos;
    if (data == null) {
      await homePageRepository.fetchPromoData();
      data = homePageRepository.promos;
    }
    return data;
  }
}
