import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/outlet.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';


class OrderOptionEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class OrderOptionStarted extends OrderOptionEvent {
  @override
  String toString() {
    return "OrderOptionStarted";
  }
}

class SelfServiceSelected extends OrderOptionEvent {
  @override
  String toString() {
    return "SelfServiceSelected";
  }
}

class DeliveryServiceSelected extends OrderOptionEvent {
  @override
  String toString() {
    return "DeliveryServiceSelected";
  }
}

class CityDropDownTapped extends OrderOptionEvent {

}

class OutletDropDownTapped extends OrderOptionEvent {

}

class CitySelected extends OrderOptionEvent {
  final City city;

  CitySelected({@required this.city});
}

class OutletSelected extends OrderOptionEvent {
  final Outlet outlet;

  OutletSelected({@required this.outlet});
}
