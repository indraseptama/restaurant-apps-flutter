import 'package:awesomeapp/blocs/order_option/bloc.dart';
import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/outlet.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class OrderOptionState extends Equatable {
  @override
  List<Object> get props => [];
}

class CardDisplayed extends OrderOptionState {
  final City city;
  final Outlet outlet;
  final TabSelected selected;
  final Function onPressOrder;

  CardDisplayed({this.city, this.outlet, this.selected, this.onPressOrder});
}

class TabChanged extends OrderOptionState {}

class CityOptionDisplayed extends OrderOptionState {
  final List<City> cities;
  CityOptionDisplayed({@required this.cities});
}

class OutletOptionDisplayed extends OrderOptionState {
  final List<Outlet> outlets;
  OutletOptionDisplayed({@required this.outlets});
}

class DialogLoading extends OrderOptionState {}

class DialogFailedToDisplay extends OrderOptionState {}

class OutletOptionFailDisplayed extends OrderOptionState {}