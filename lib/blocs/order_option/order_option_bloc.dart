import 'package:awesomeapp/repositories/home_page_repository.dart';
import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/outlet.dart';
import 'package:awesomeapp/repositories/api/app_exceptions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc.dart';

enum TabSelected { SelfService, DeliveryService }

class OrderOptionBloc extends Bloc<OrderOptionEvent, OrderOptionState> {
  City city;
  Outlet outlet;
  TabSelected selected = TabSelected.DeliveryService;

  final HomePageRepository homePageRepository;

  OrderOptionBloc({this.homePageRepository})
      : assert(homePageRepository != null);

  @override
  OrderOptionState get initialState => TabChanged();

  @override
  Stream<OrderOptionState> mapEventToState(OrderOptionEvent event) async* {
    if (event is OrderOptionStarted) {
      yield* _mapOrderOptionStartedToState();
    }
    if (event is SelfServiceSelected) {
      yield* _mapSelfServiceSelectedToState();
    }
    if (event is DeliveryServiceSelected) {
      yield* _mapDeliveryServiceSelectedToState();
    }
    if (event is CityDropDownTapped) {
      yield* _mapCityDropDownTappedToState();
    }
    if (event is OutletDropDownTapped) {
      yield* _mapOutletDropDownTappedToState();
    }
    if (event is CitySelected) {
      yield* _mapCitySelectedToState(event);
    }
    if (event is OutletSelected) {
      yield* _mapOutletSelectedToState(event);
    }
  }

  Stream<OrderOptionState> _mapOrderOptionStartedToState() async* {
    yield TabChanged();
    yield CardDisplayed(city: city, outlet: outlet, selected: selected);
  }

  Stream<OrderOptionState> _mapSelfServiceSelectedToState() async* {
    this.selected = TabSelected.SelfService;
    yield TabChanged();
    yield CardDisplayed(
        city: this.city, outlet: this.outlet, selected: this.selected);
  }

  Stream<OrderOptionState> _mapDeliveryServiceSelectedToState() async* {
    this.selected = TabSelected.DeliveryService;
    yield TabChanged();
    yield CardDisplayed(
        city: this.city, outlet: this.outlet, selected: this.selected);
  }

  Stream<OrderOptionState> _mapCityDropDownTappedToState() async* {
    yield DialogLoading();
    try {
      List<City> data = await _getHomePageCityData();
      print("getData");
      yield CityOptionDisplayed(cities: data);
    } on FetchDataException {
      yield DialogFailedToDisplay();
    } on BadRequestException {
      yield DialogFailedToDisplay();
    } on UnauthorisedException {
      yield DialogFailedToDisplay();
    } on InvalidInputException {
      yield DialogFailedToDisplay();
    }
  }

  Stream<OrderOptionState> _mapOutletDropDownTappedToState() async* {
    if (this.city != null) {
      yield DialogLoading();
      yield OutletOptionDisplayed(outlets: city.outlets);
    } else {
      yield OutletOptionFailDisplayed();
    }
  }

  Stream<OrderOptionState> _mapCitySelectedToState(CitySelected event) async* {
    this.city = event.city;
    if (city == null) {
      outlet = null;
    }
    yield TabChanged();
    yield CardDisplayed(
        city: this.city, outlet: this.outlet, selected: this.selected);
  }

  Stream<OrderOptionState> _mapOutletSelectedToState(
      OutletSelected event) async* {
    this.outlet = event.outlet;
    yield TabChanged();
    yield CardDisplayed(
        city: this.city, outlet: this.outlet, selected: this.selected);
  }

  Future<List<City>> _getHomePageCityData() async {
    List<City> data = homePageRepository.cities;
    if (data == null) {
      await homePageRepository.fetchCityData();
      data = homePageRepository.cities;
    }
    return data;
  }
}
