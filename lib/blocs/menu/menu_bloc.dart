import 'package:awesomeapp/blocs/menu/menu_event.dart';
import 'package:awesomeapp/blocs/menu/menu_state.dart';
import 'package:awesomeapp/repositories/menu_page_repository.dart';
import 'package:awesomeapp/repositories/models/menu_product.dart';
import 'package:awesomeapp/repositories/models/product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:awesomeapp/repositories/api/app_exceptions.dart';

class MenuBloc extends Bloc<MenuEvent, MenuState> {
  MenuPageRepository menuPageRepository;
  List<MenuProduct> menuProduct;
  List<Product> productInCart;
  MenuBloc({this.menuPageRepository}) : assert(menuPageRepository != null);

  @override
  MenuState get initialState => MenuLoading();

  @override
  Stream<MenuState> mapEventToState(MenuEvent event) async* {
    if (event is MenuPageStarted) {
      try {
        List<MenuProduct> menu = await _getMenuProductData();
        List<Product> productInCart = await _getProductInCartData();

        yield MenuLoading();
        yield MenuLoaded(menuProducts: menu, productInCart: productInCart);
      } on FetchDataException {
        yield MenuProductFailedToDisplay();
      } on BadRequestException {
        yield MenuProductFailedToDisplay();
      } on UnauthorisedException {
        yield MenuProductFailedToDisplay();
      } on InvalidInputException {
        yield MenuProductFailedToDisplay();
      }
    }
  }

  Future<List<MenuProduct>> _getMenuProductData() async {
    List<MenuProduct> data = menuPageRepository.menuProducts;
    if (data == null) {
      await menuPageRepository.fetchMenuData();
      data = menuPageRepository.menuProducts;
    }
    return data;
  }

  Future<List<Product>> _getProductInCartData() async {
    List<Product> data = menuPageRepository.productsInCart;
    if (data == null) {
      await menuPageRepository.fetchProductInCart();
      data = menuPageRepository.productsInCart;
    }
    return data;
  }
}
