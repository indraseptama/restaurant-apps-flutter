import 'package:awesomeapp/repositories/models/menu_product.dart';
import 'package:awesomeapp/repositories/models/product.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MenuState extends Equatable {
  @override
  List<Object> get props => [];
}

class MenuLoading extends MenuState {
  @override
  String toString() {
    return "MenuLoading";
  }
}

class MenuLoaded extends MenuState {
  final List<MenuProduct> menuProducts;
  final List<Product> productInCart;

  MenuLoaded({@required this.menuProducts, this.productInCart});

  @override
  List<Object> get props => [menuProducts, productInCart];

  @override
  String toString() {
    return "MenuLoaded";
  }
}

class MenuProductFailedToDisplay extends MenuState {}
