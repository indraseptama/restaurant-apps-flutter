import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class BottomNavigationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class HomePageStarted extends BottomNavigationEvent {
  @override
  String toString() {
    return "AppStarted";
  }
}

class PageTapped extends BottomNavigationEvent {
  final int index;

  PageTapped({@required this.index});

  @override
  List<Object> get props => [index];

  @override
  String toString() {
    return 'PageTapped: $index';
  }
}
