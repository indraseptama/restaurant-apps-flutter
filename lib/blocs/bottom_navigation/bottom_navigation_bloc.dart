import 'package:awesomeapp/repositories/history_page_repository.dart';
import 'package:awesomeapp/repositories/home_page_repository.dart';
import 'package:awesomeapp/repositories/models/promo.dart';
import 'package:awesomeapp/repositories/profile_page_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc.dart';

class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {
  final HomePageRepository homePageRepository;
  final HistoryPageRepository historyPageRepository;
  final ProfilePageRepository profilePageRepository;

  int currentIndex = 0;

  BottomNavigationBloc(
      {this.homePageRepository,
      this.historyPageRepository,
      this.profilePageRepository})
      : assert(homePageRepository != null),
        assert(historyPageRepository != null),
        assert(profilePageRepository != null);

  @override
  BottomNavigationState get initialState => PageLoading();

  @override
  Stream<BottomNavigationState> mapEventToState(
      BottomNavigationEvent event) async* {
    if (event is HomePageStarted) {
      this.add(PageTapped(index: this.currentIndex));
    }
    if (event is PageTapped) {
      yield* _mapPageTappedToState(event);
    }
  }

  Stream<BottomNavigationState> _mapPageTappedToState(PageTapped event) async* {
    this.currentIndex = event.index;

    yield CurrentIndexChanged(currentIndex: this.currentIndex);
    yield PageLoading();

    if (this.currentIndex == 0) {
      String data = await _getHomePageData();
      List<Promo> promos = await _getHomePagePromoData();
      yield HomePageLoaded(text: data, promos: promos);
    }

    if (this.currentIndex == 1) {
      int data = await _getHistoryPageData();
      yield HistoryPageLoaded(number: data);
    }

    if (this.currentIndex == 2) {
      int data = await _getProfilePageData();
      yield ProfilePageLoaded(number: data);
    }
  }

  Future<String> _getHomePageData() async {
    String data = homePageRepository.data;
    if (data == null) {
      await homePageRepository.fetchData();
      data = homePageRepository.data;
    }
    return data;
  }

  Future<List<Promo>> _getHomePagePromoData() async {
    List<Promo> data = homePageRepository.promos;
    if (data == null) {
      await homePageRepository.fetchPromoData();
      data = homePageRepository.promos;
    }
    return data;
  }

  Future<int> _getHistoryPageData() async {
    int data = historyPageRepository.data;
    if (data == null) {
      await historyPageRepository.fetchData();
      data = historyPageRepository.data;
    }
    return data;
  }

  Future<int> _getProfilePageData() async {
    int data = profilePageRepository.data;
    if (data == null) {
      await profilePageRepository.fetchData();
      data = profilePageRepository.data;
    }
    return data;
  }
}
