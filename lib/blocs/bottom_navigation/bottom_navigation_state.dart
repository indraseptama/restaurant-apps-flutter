import 'package:awesomeapp/repositories/models/promo.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BottomNavigationState extends Equatable{
  @override
  List<Object> get props => [];
}

class CurrentIndexChanged extends BottomNavigationState{
  final int currentIndex;

  CurrentIndexChanged({@required this.currentIndex});

  @override
  List<Object> get props => [currentIndex];

  @override
  String toString() {
    return 'CurrentIndexChanged : $currentIndex';
  }
}

class PageLoading extends BottomNavigationState{
  @override
  String toString() {
    return 'PageLoading';
  }
}

class HomePageLoaded extends BottomNavigationState{
  final String text;
  final List<Promo> promos;

  HomePageLoaded({@required this.text, @required this.promos});

  @override
  List<Object> get props => [text, promos];

  @override
  String toString() {
    return 'HomePageLoaded with text: $text';
  }
}

class ProfilePageLoaded extends BottomNavigationState{
  final int number;

  ProfilePageLoaded({@required this.number});

  @override
  List<Object> get props => [number];

  @override
  String toString() {
    return 'ProfilePageLoaded with number: $number';
  }
}

class HistoryPageLoaded extends BottomNavigationState{
  final int number;

  HistoryPageLoaded({@required this.number});

  @override
  List<Object> get props => [number];

  @override
  String toString() {
    return 'HistoryPageLoaded with number: $number';
  }
}
