import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:awesomeapp/repositories/api/google_maps_requests.dart';
import 'package:google_maps_webservice/places.dart';

class MyMapPage extends StatefulWidget {
  @override
  _MyMapPageState createState() => _MyMapPageState();
}

class _MyMapPageState extends State<MyMapPage> {
  GoogleMapController mapController;
  GoogleMapsServices _googleMapsServices = GoogleMapsServices();
  TextEditingController locationController = TextEditingController();
  TextEditingController destinationController = TextEditingController();
  FocusNode destinationFocusNode = FocusNode();
  static const kGoogleApiKey = "AIzaSyB_FVCWZpiBCVu3zto2rn8yXYOiDRCIlV0";
  static LatLng _initLatLng;
  LatLng _lastLatLng = _initLatLng;

  final Set<Marker> _markers = {};
  final Set<Polyline> _polyLines = {};

  String apiKey;

  @override
  void dispose() {
    destinationFocusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _getUserLocation();
    if (Platform.isAndroid) {
      apiKey = "AIzaSyCa6ND8mwSxWX4wa4PbJmrMMH569bMBhAo";
    } else {
      apiKey = "AIzaSyBmkG8UWwI_qaqAK5dGuEzxhtQQX_EaNbs";
    }
  }

  @override
  Widget build(BuildContext context) {
    return _initLatLng == null
        ? Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Scaffold(
            body: Stack(
              children: <Widget>[
                GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition:
                      CameraPosition(target: _initLatLng, zoom: 15),
                  onMapCreated: onCreated,
                  myLocationEnabled: true,
                  compassEnabled: true,
                  markers: _markers,
                  polylines: _polyLines,
                  onCameraMove: _onCameraMove,
                ),
                Positioned(
                  top: 50,
                  right: 15,
                  left: 15,
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey,
                              offset: Offset(1.0, 5.0),
                              blurRadius: 10,
                              spreadRadius: 3)
                        ]),
                    child: TextField(
                      controller: locationController,
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          icon: Container(
                            margin: EdgeInsets.only(left: 15, bottom: 15),
                            width: 10,
                            height: 10,
                            child: Icon(
                              Icons.location_on,
                              color: Colors.black,
                            ),
                          ),
                          hintText: "pick up",
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 15)),
                    ),
                  ),
                ),
                Positioned(
                  top: 110,
                  right: 15,
                  left: 15,
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey,
                              offset: Offset(1.0, 5.0),
                              blurRadius: 10,
                              spreadRadius: 3)
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: TextField(
                        readOnly: true,
                        cursorColor: Colors.black,
                        focusNode: destinationFocusNode,
                        controller: destinationController,
                        onTap: () async {
                          Prediction p = await PlacesAutocomplete.show(
                              context: context,
                              strictbounds: true,
                              location: Location(
                                  _initLatLng.latitude, _initLatLng.longitude),
                              apiKey: kGoogleApiKey,
                              onError: onError,
                              radius: 100000,
                              mode: Mode.overlay);

                          if (p != null) {
                            sendRequest(p.description);
                            FocusScope.of(context).requestFocus(FocusNode());
                            destinationController.text = p.description;
                          }
                        },
                        decoration: InputDecoration(
                            icon: Container(
                              margin: EdgeInsets.only(left: 15, bottom: 15),
                              width: 10,
                              height: 10,
                              child: Icon(
                                Icons.local_taxi,
                                color: Colors.black,
                              ),
                            ),
                            hintText: "destination",
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(left: 15)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
  }

  void onCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  void _onCameraMove(CameraPosition position) {
    setState(() {
      _lastLatLng = position.target;
    });
  }

  void _addMarker(LatLng location, String address) {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_lastLatLng.toString()),
          position: location,
          infoWindow: InfoWindow(title: address, snippet: "go here"),
          icon: BitmapDescriptor.defaultMarker));
    });
  }

  void createRoute(String encodedPoly) {
    setState(() {
      _polyLines.add(Polyline(
          polylineId: PolylineId(_lastLatLng.toString()),
          width: 10,
          color: Colors.blue,
          points: convertToLatLng(_decodePoly(encodedPoly))));
    });
  }

  // Convert list of doubles into LatLng
  List<LatLng> convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }

    return result;
  }

  // !DECODE POLY
  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
// repeating until all attributes are decoded
    do {
      var shift = 0;
      int result = 0;

      // for decoding value of one attribute
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      /* if value is negetive then bitwise not the value */
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

/*adding to previous value as done in encoding */
    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }

  void _getUserLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    List<Placemark> placemarks = await Geolocator()
        .placemarkFromCoordinates(position.latitude, position.longitude);

    setState(() {
      _initLatLng = LatLng(position.latitude, position.longitude);
      locationController.text = placemarks[0].name;
    });
  }

  void sendRequest(String intendedLocation) async {
    List<Placemark> placemark =
        await Geolocator().placemarkFromAddress(intendedLocation);
    double latitude = placemark[0].position.latitude;
    double longitude = placemark[0].position.longitude;
    LatLng destination = LatLng(latitude, longitude);
    _addMarker(destination, intendedLocation);
    String route =
        await _googleMapsServices.getRouteCoordinates(_initLatLng, destination);
    createRoute(route);
  }

  void onError(PlacesAutocompleteResponse response) {
    print("error with respon ${response.errorMessage}");
  }
}
