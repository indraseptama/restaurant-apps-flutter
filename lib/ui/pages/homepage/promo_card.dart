import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PromoCard extends StatelessWidget {
  final String name;
  final int price;
  final String description;
  final String imageUrl;
  final bool isActive;
  final bool isFavorite;

  final Function onTapCard;
  final Function onTapFavoriteButton;

  const PromoCard(
      {Key key,
      @required this.name,
      @required this.price,
      @required this.description,
      @required this.imageUrl,
      this.onTapCard,
      @required this.isActive,
      @required this.isFavorite,
      @required this.onTapFavoriteButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double topBottom = this.isActive ? 10 : 20;
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      padding: EdgeInsets.only(left: 10, right: 10, top: topBottom, bottom: topBottom),
      child: Container(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black38,
                        offset: Offset(2.0, 2.0),
                        blurRadius: 5.0,
                        spreadRadius: 1.0)
                  ]),
            ),
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              child: Image(
                image: NetworkImage(imageUrl),
                fit: BoxFit.cover,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.transparent, Colors.black])),
            ),
            Container(
              child: Align(
                  alignment: Alignment.topLeft,
                  child: FloatingActionButton(
                    heroTag: null,
                    mini: true,
                    backgroundColor: Colors.grey.withOpacity(0.5),
                    onPressed: this.onTapFavoriteButton,
                    child: Icon(
                      this.isFavorite ? Icons.favorite : Icons.favorite_border,
                      color: this.isFavorite ? Colors.red : Colors.black,
                    ),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.name,
                            textAlign: TextAlign.start,
                            maxLines: 2,
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.white),
                          )),
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Text(
                            this.price.toString(),
                            textAlign: TextAlign.right,
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                  Text(
                    this.description,
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 12.0, color: Colors.white),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
