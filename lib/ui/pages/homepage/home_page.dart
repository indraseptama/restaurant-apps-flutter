import 'package:awesomeapp/blocs/promo/bloc.dart';
import 'package:awesomeapp/blocs/order_option/bloc.dart';
import 'package:awesomeapp/repositories/home_page_repository.dart';
import 'package:awesomeapp/ui/custom_widget/routes.dart';
import 'package:awesomeapp/ui/pages/homepage/order_option_card.dart';
import 'package:awesomeapp/ui/pages/homepage/page_all_promos.dart';
import 'package:awesomeapp/ui/pages/homepage/promo_swiper.dart';
import 'package:awesomeapp/ui/pages/homepage/test_map.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePage extends StatefulWidget {
  final HomePageRepository _homePageRepository;

  const HomePage({Key key, @required HomePageRepository homePageRepository})
      : _homePageRepository = homePageRepository,
        super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final SwiperController controller = SwiperController();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.mail),
        ),
        title: Text("Selamat Datang, ${widget._homePageRepository.data}"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {},
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 15, top: 5, bottom: 15),
              child: Text(
                "Promo",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              )),
          BlocProvider<PromoBloc>(
              create: (context) =>
                  PromoBloc(homePageRepository: widget._homePageRepository)
                    ..add(PageStarted()),
              child: Container(
                height: width * 9 / 16,
                width: width,
                child: PromoSwiper(),
              )),
          Align(
            alignment: Alignment.centerRight,
            child: FlatButton(
              onPressed: () {
                Navigator.push(context, SlideLeftRoute(page: PageAllPromos()));
              },
              child: Text("Lihat Semua Promo"),
            ),
          ),
          BlocProvider<OrderOptionBloc>(
            create: (context) =>
                OrderOptionBloc(homePageRepository: widget._homePageRepository)
                  ..add(OrderOptionStarted()),
            child: OrderOptionCard(),
          ),
          FlatButton(
            onPressed: () {
              Navigator.push(context, SlideLeftRoute(page: MyMapPage()));
            },
            child: Text("Coba Map"),
          )
        ],
      ),
    );
  }
}
