import 'package:awesomeapp/ui/pages/homepage/promo_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:awesomeapp/blocs/promo/bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class PromoSwiper extends StatelessWidget {
  const PromoSwiper({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final PromoBloc promoBloc = BlocProvider.of<PromoBloc>(context);
    return BlocBuilder<PromoBloc, PromoState>(builder: (context, state) {
      if (state is PromoLoading) {
        return Center(child: CircularProgressIndicator());
      } else {
        return Swiper(
          viewportFraction: 0.8,
          layout: SwiperLayout.DEFAULT,
          itemCount: promoBloc.promos.length,
          scrollDirection: Axis.horizontal,
          pagination: new SwiperPagination(),
          onIndexChanged: (index) {
            promoBloc.add(PromoSwiped(index: index));
          },
          itemBuilder: (context, currentIndex) {
            return Center(
              child: PromoCard(
                name: promoBloc.promos[currentIndex].name,
                price: promoBloc.promos[currentIndex].price,
                imageUrl: promoBloc.promos[currentIndex].imageUrl,
                description: promoBloc.promos[currentIndex].description,
                isActive: promoBloc.currentIndex == currentIndex ? true : false,
                isFavorite: promoBloc.promos[currentIndex].isFavorite,
                onTapCard: () {},
                onTapFavoriteButton: () {
                  promoBloc.add(FavoriteButtonTapped(
                      isFavorite: !promoBloc.promos[currentIndex].isFavorite,
                      index: currentIndex));
                },
              ),
            );
          },
        );
      }
    });
  }
}
