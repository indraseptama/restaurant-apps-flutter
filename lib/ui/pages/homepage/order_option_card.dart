import 'package:awesomeapp/blocs/menu/bloc.dart';
import 'package:awesomeapp/repositories/menu_page_repository.dart';
import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/outlet.dart';
import 'package:awesomeapp/ui/custom_widget/routes.dart';
import 'package:awesomeapp/ui/pages/menupage/menu_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:awesomeapp/blocs/order_option/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OrderOptionCard extends StatelessWidget {
  const OrderOptionCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final OrderOptionBloc optionOrderBloc =
        BlocProvider.of<OrderOptionBloc>(context);
    final width = MediaQuery.of(context).size.width;
    final GlobalKey<State> _keyLoader = new GlobalKey<State>();

    return BlocListener(
      bloc: optionOrderBloc,
      listener: (context, state) {
        if (state is CityOptionDisplayed) {
          if (_keyLoader.currentContext != null) {
            Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          }

          WidgetsBinding.instance.addPostFrameCallback((_) =>
              _asyncCityOptionDialog(context, optionOrderBloc, state.cities));
        }

        if (state is OutletOptionDisplayed) {
          if (_keyLoader.currentContext != null) {
            Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          }

          WidgetsBinding.instance.addPostFrameCallback((_) =>
              _asyncOutletOptionDialog(
                  context, optionOrderBloc, state.outlets));
        }

        if (state is DialogLoading) {
          WidgetsBinding.instance.addPostFrameCallback(
              (_) => _showLoadingDialog(context, _keyLoader));
        }

        if (state is DialogFailedToDisplay) {
          WidgetsBinding.instance.addPostFrameCallback(
              (_) => _asyncOptionDialogFailDisplayed(context, optionOrderBloc));
        }

        if (state is OutletOptionFailDisplayed) {
          WidgetsBinding.instance.addPostFrameCallback((_) =>
              _asyncOptionDialogOutletFailDisplayed(context, optionOrderBloc));
        }
      },
      child: BlocBuilder<OrderOptionBloc, OrderOptionState>(
          builder: (context, state) {
        return Center(
          child: Column(
            children: <Widget>[
              buildOptionBox(width, optionOrderBloc),
              Padding(
                padding: const EdgeInsets.only(right: 50),
                child: Align(
                    alignment: Alignment.centerRight,
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                      ),
                      onPressed: () {
                        if (optionOrderBloc.city != null &&
                            optionOrderBloc.outlet != null) {
                          Navigator.push(
                              context,
                              SlideLeftRoute(
                                  page: BlocProvider<MenuBloc>(
                                create: (context) => MenuBloc(
                                    menuPageRepository: MenuPageRepository())
                                  ..add(MenuPageStarted()),
                                child: MenuPage(
                                  city: optionOrderBloc.city,
                                  outlet: optionOrderBloc.outlet,
                                ),
                              )));
                        } else {
                          _asyncAlertDialog(context, optionOrderBloc);
                        }
                      },
                      highlightColor: Colors.transparent,
                      color: Colors.red,
                      child: Text(
                        "Pesan Sekarang",
                        style: TextStyle(color: Colors.white),
                      ),
                    )),
              ),
            ],
          ),
        );
      }),
    );
  }

  Container buildOptionBox(double width, OrderOptionBloc optionOrderBloc) {
    return Container(
      height: 160,
      width: width * 3 / 4,
      margin: EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 6,
                offset: Offset(1, 1))
          ]),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    height: 40,
                    decoration: BoxDecoration(
                      color: optionOrderBloc.selected ==
                              TabSelected.DeliveryService
                          ? Colors.white
                          : Colors.red,
                    ),
                    child: FlatButton(
                        onPressed: () {
                          optionOrderBloc.add(DeliveryServiceSelected());
                        },
                        child: Text(
                          "Dikirim",
                          style: TextStyle(
                              color: optionOrderBloc.selected ==
                                      TabSelected.DeliveryService
                                  ? Colors.red
                                  : Colors.white),
                          textAlign: TextAlign.center,
                        )),
                  )),
              Expanded(
                  flex: 1,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    height: 40,
                    decoration: BoxDecoration(
                      color: optionOrderBloc.selected == TabSelected.SelfService
                          ? Colors.white
                          : Colors.red,
                    ),
                    child: FlatButton(
                        onPressed: () {
                          optionOrderBloc.add(SelfServiceSelected());
                        },
                        child: Text(
                          "Ambil Sendiri",
                          style: TextStyle(
                              color: optionOrderBloc.selected ==
                                      TabSelected.SelfService
                                  ? Colors.red
                                  : Colors.white),
                          textAlign: TextAlign.center,
                        )),
                  )),
            ],
          ),
          Container(
            height: 120,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          "Pilih Kota",
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                    ),
                    Expanded(
                        flex: 5,
                        child: FlatButton(
                            onPressed: () {
                              optionOrderBloc.add(CityDropDownTapped());
                            },
                            child: ListTile(
                              contentPadding: EdgeInsets.only(left: 5),
                              title: optionOrderBloc.city != null
                                  ? Text(optionOrderBloc.city.name)
                                  : Text("Kota"),
                              trailing: Icon(Icons.keyboard_arrow_down),
                            )))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Text(
                            "Pilih Outlet",
                            style: TextStyle(color: Colors.red),
                          ),
                        )),
                    Expanded(
                        flex: 5,
                        child: FlatButton(
                            onPressed: () {
                              optionOrderBloc.add(OutletDropDownTapped());
                            },
                            child: ListTile(
                              contentPadding: EdgeInsets.only(left: 5),
                              title: optionOrderBloc.outlet != null
                                  ? Text(optionOrderBloc.outlet.name)
                                  : Text("outlet"),
                              trailing: Icon(Icons.keyboard_arrow_down),
                            )))
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future _asyncCityOptionDialog(BuildContext context,
      OrderOptionBloc orderOptionBloc, List<City> cities) async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () {
              orderOptionBloc.add(CitySelected(city: null));
              return Future.value(true);
            },
            child: AlertDialog(
                title: Text('Pilih kota'),
                content: Container(
                    width: 300,
                    height: 300,
                    child: ListView.builder(
                        itemCount: cities.length,
                        itemBuilder: (context, currentIndex) {
                          return ListTile(
                            onTap: () {
                              orderOptionBloc.add(
                                  CitySelected(city: cities[currentIndex]));
                              Navigator.pop(context);
                            },
                            title: Text(cities[currentIndex].name),
                          );
                        }))),
          );
        });
  }

  Future _asyncOutletOptionDialog(BuildContext context,
      OrderOptionBloc orderOptionBloc, List<Outlet> outlets) async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () {
              orderOptionBloc.add(OutletSelected(outlet: null));
              return Future.value(true);
            },
            child: AlertDialog(
                title: Text('Pilih outlet'),
                content: Container(
                    width: 300,
                    height: 300,
                    child: ListView.builder(
                        itemCount: outlets.length,
                        itemBuilder: (context, currentIndex) {
                          return ListTile(
                            onTap: () {
                              orderOptionBloc.add(OutletSelected(
                                  outlet: outlets[currentIndex]));
                              Navigator.pop(context);
                            },
                            title: Text(outlets[currentIndex].name),
                          );
                        }))),
          );
        });
  }

  static Future _showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Mohon tunggu sebentar....",
                          style: TextStyle(color: Colors.blueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }

  Future _asyncOptionDialogFailDisplayed(
      BuildContext context, OrderOptionBloc orderOptionBloc) async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () {
                orderOptionBloc.add(CitySelected(city: null));
                return Future.value(true);
              },
              child: AlertDialog(
                title: Text("Daftar Kota Gagal Ditampilkan"),
                content: Text("Periksa Jaringan Anda"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Tutup"),
                    onPressed: () {
                      orderOptionBloc.add(CitySelected(city: null));
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
        });
  }

  Future _asyncOptionDialogOutletFailDisplayed(
      BuildContext context, OrderOptionBloc orderOptionBloc) async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () {
                orderOptionBloc.add(CitySelected(city: null));
                return Future.value(true);
              },
              child: AlertDialog(
                title: Text("Daftar Outlet Gagal Ditampilkan"),
                content: Text("Pilih Kota terlebih dahulu"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Tutup"),
                    onPressed: () {
                      orderOptionBloc.add(CitySelected(city: null));
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
        });
  }

  Future _asyncAlertDialog(
      BuildContext context, OrderOptionBloc orderOptionBloc) async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () {
                orderOptionBloc.add(CitySelected(city: null));
                return Future.value(true);
              },
              child: AlertDialog(
                title: Text("Tidak Dapat Memesan"),
                content: Text("Pilih Kota dan Outlet Terlebih dahulu"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Tutup"),
                    onPressed: () {
                      orderOptionBloc.add(CitySelected(city: null));
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
        });
  }
}
