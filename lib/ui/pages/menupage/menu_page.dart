import 'package:awesomeapp/blocs/menu/bloc.dart';
import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/outlet.dart';
import 'package:awesomeapp/ui/pages/menupage/menu_group.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MenuPage extends StatelessWidget {
  final City city;
  final Outlet outlet;

  const MenuPage({Key key, @required this.city, @required this.outlet})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MenuBloc optionOrderBloc = BlocProvider.of<MenuBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "Dikirim dari ${city.name}, ${outlet.name}",
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        body: BlocBuilder<MenuBloc, MenuState>(builder: (context, state) {
          if (state is MenuLoaded) {
            return ListView.builder(
                itemCount: state.menuProducts.length,
                itemBuilder: (context, currentIndex) {
                  return MenuGroupInterface(
                      menuProduct: state.menuProducts[currentIndex]);
                });
          }
          return Center(child: CircularProgressIndicator());
        }));
  }
}
