import 'package:awesomeapp/repositories/models/menu_product.dart';
import 'package:awesomeapp/ui/custom_widget/column_builder.dart';
import 'package:flutter/material.dart';

class MenuGroupInterface extends StatelessWidget {
  final MenuProduct menuProduct;

  const MenuGroupInterface({Key key, this.menuProduct}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Text(
              menuProduct.name,
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline),
            ),
          ),
          ColumnBuilder(
              itemCount: menuProduct.products.length,
              itemBuilder: (context, currentIndex) {
                return Container(
                  padding: EdgeInsets.all(2.0),
                  margin: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 2.0,
                            blurRadius: 5.0)
                      ]),
                  child: FlatButton(
                    onPressed: () {},
                    padding: EdgeInsets.all(0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          child: Image(
                            image: NetworkImage(
                                menuProduct.products[currentIndex].imageUrl),
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              menuProduct.products[currentIndex].name,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(
                              menuProduct.products[currentIndex].price
                                  .toString(),
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold)),
                        )
                      ],
                    ),
                  ),
                );
              })
        ],
      ),
    );
  }
}
