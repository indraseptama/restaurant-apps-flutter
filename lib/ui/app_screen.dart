import 'package:flutter/material.dart';
import 'package:awesomeapp/blocs/bottom_navigation/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'pages/pages.dart';

class AppScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BottomNavigationBloc bottomNavigationBloc =
        BlocProvider.of<BottomNavigationBloc>(context);

    final PageStorageBucket bucket = PageStorageBucket();

    return Scaffold(
      body: BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
        builder: (context, state) {
          if (state is PageLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is HomePageLoaded) {
            return HomePage(
              key: PageStorageKey('HomePage'),
              homePageRepository: bottomNavigationBloc.homePageRepository,
            );
          } else if (state is HistoryPageLoaded) {
            return HistoryPage(
              key: PageStorageKey('HistoryPage'),

            );
          } else if (state is ProfilePageLoaded) {
            return ProfilePage(
              key: PageStorageKey('ProfilePage'),
            );
          }
          return Container();
        },
      ),
      bottomNavigationBar:
          BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
        bloc: bottomNavigationBloc,
        builder: (context, state) {
          return BottomNavigationBar(
            currentIndex: bottomNavigationBloc.currentIndex,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home, color: Colors.black),
                title: Text('Home'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.history, color: Colors.black),
                title: Text('History'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person_pin, color: Colors.black),
                title: Text('Profile'),
              ),
            ],
            onTap: (index) =>
                bottomNavigationBloc.add(PageTapped(index: index)),
          );
        },
      ),
    );
  }
}
