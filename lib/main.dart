import 'package:awesomeapp/repositories/history_page_repository.dart';
import 'package:awesomeapp/repositories/home_page_repository.dart';
import 'package:awesomeapp/repositories/profile_page_repository.dart';
import 'package:awesomeapp/repositories/user_repository.dart';
import 'package:awesomeapp/ui/app_screen.dart';
import 'package:awesomeapp/ui/pages/loginpage/login_screen.dart';
import 'package:awesomeapp/ui/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'blocs/authentication/authentication_bloc.dart';
import 'blocs/bottom_navigation/bloc.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}

void main() {
  final UserRepository userRepository = UserRepository();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(BlocProvider(
    create: (context) =>
        AuthenticationBloc(userRepository: userRepository)..add(AppStarted()),
    child: App(
      userRepository: userRepository,
    ),
  ));
}

class App extends StatelessWidget {
  final UserRepository _userRepository;

  App({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.red),
      home: SafeArea(
        child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            print("Hehe " + state.toString());
            if (state is Authenticated) {
              return BlocProvider<BottomNavigationBloc>(
                create: (context) => BottomNavigationBloc(
                    homePageRepository: HomePageRepository(),
                    historyPageRepository: HistoryPageRepository(),
                    profilePageRepository: ProfilePageRepository())
                  ..add(HomePageStarted()),
                child: AppScreen(),
              );
            }
            if (state is Unauthenticated) {
              return LoginScreen(
                userRepository: _userRepository,
              );
            }
            return SplashScreen();
          },
        ),
      ),
    );
  }
}
