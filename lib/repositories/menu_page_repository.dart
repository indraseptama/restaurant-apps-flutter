import 'dart:convert';

import 'package:awesomeapp/repositories/api/api_service.dart';
import 'package:awesomeapp/repositories/local_db/database_provider.dart';

import 'models/menu_product.dart';
import 'models/product.dart';
import 'package:http/http.dart' as http;

class MenuPageRepository {
  List<MenuProduct> _menuProducts;
  List<Product> _productsInCart;
  ApiService apiService;

  MenuPageRepository() {
    apiService = ApiService();
  }

  Future<void> fetchMenuData() async {
    _menuProducts = await apiService.getMenuProducts();
  }

  Future<void> fetchProductInCart() async {
    _productsInCart = await DatabaseProvider.db.getProducts();
  }

  Future<Product> insertProductInCart(Product product) async {
    Product addedProduct = await DatabaseProvider.db.insert(product);

    if (addedProduct != null) {
      _productsInCart.add(addedProduct);
    }

    return addedProduct;
  }

  Future<bool> updateProductInCart(Product product) async {
    int updatedId = await DatabaseProvider.db.update(product);
    bool isUpdated = false;

    if (updatedId != null) {
      _productsInCart.remove(_productsInCart
          .singleWhere((p) => p.id == updatedId, orElse: () => null));
      _productsInCart.add(product);

      isUpdated = true;
    }

    return isUpdated;
  }

  Future<bool> deleteProductInCart(Product product) async {
    int deletedId = await DatabaseProvider.db.delete(product.id);
    bool isDeleted = false;
    if (deletedId != null) {
      _productsInCart.remove(_productsInCart
          .singleWhere((p) => p.id == deletedId, orElse: () => null));

      isDeleted = true;
    }

    return isDeleted;
  }

  List<MenuProduct> get menuProducts => _menuProducts;
  List<Product> get productsInCart => _productsInCart;
}
