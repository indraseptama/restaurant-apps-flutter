import 'package:awesomeapp/repositories/api/api_service.dart';
import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/promo.dart';

class HomePageRepository {
  String _data;
  List<Promo> _promos;
  List<City> _cities;
  ApiService apiService;

  HomePageRepository() {
    apiService = ApiService();
  }

  Future<void> fetchData() async {
    await Future.delayed(Duration(milliseconds: 500));
    _data = 'Indra';
  }

  Future<void> fetchPromoData() async {
    _promos = await apiService.getPromos();
  }

  Future<void> fetchCityData() async {
    _cities = await apiService.getCities();
  }

  String get data => _data;
  List<Promo> get promos => _promos;
  List<City> get cities => _cities;
}
