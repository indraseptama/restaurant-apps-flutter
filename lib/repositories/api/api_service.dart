import 'dart:convert';
import 'dart:io';
import 'package:awesomeapp/repositories/api/app_exceptions.dart';
import 'package:awesomeapp/repositories/models/city.dart';
import 'package:awesomeapp/repositories/models/menu_product.dart';
import 'package:awesomeapp/repositories/models/promo.dart';
import 'package:http/http.dart' as http;

class ApiService {
  final String baseUrl = "https://demo7494351.mockable.io";

  Future<List<MenuProduct>> getMenuProducts() async {
    try {
      String apiURL = "$baseUrl/getProductList";
      List<MenuProduct> _menuProducts = List<MenuProduct>();
      var apiResult = await http.get(apiURL);
      var jsonObject = _returnResponse(apiResult);

      List<dynamic> listMenuProduct =
          (jsonObject as Map<String, dynamic>)['data'];

      for (dynamic menuJson in listMenuProduct) {
        MenuProduct tmp = MenuProduct.createMenuProductFromJson(menuJson);
        _menuProducts.add(tmp);
      }
      return _menuProducts;
    } on SocketException {
      throw FetchDataException();
    }
  }

  Future<List<City>> getCities() async {
    try {
      String apiURL = "$baseUrl/getCityLIst";
      var apiResult = await http.get(apiURL);
      var jsonObject = _returnResponse(apiResult);

      List<dynamic> listCity = (jsonObject as Map<String, dynamic>)['data'];
      List<City> _cities = List<City>();
      for (dynamic cityJson in listCity) {
        City tmp = City.createCityFromJson(cityJson);
        _cities.add(tmp);
      }
      return _cities;
    } on SocketException {
      throw FetchDataException();
    }
  }

  Future<List<Promo>> getPromos() async {
    try {
      List<Promo> _promos = List<Promo>();
      // store dummy data
      _promos.add(Promo(
          name: "Makanan 1",
          price: 28000,
          imageUrl:
              "https://img.okezone.com/content/2019/04/26/298/2048384/5-makanan-yang-lebih-enak-beli-di-luar-daripada-masak-di-rumah-aVhG25pSQF.jpg",
          description: "heheheh",
          isFavorite: false));
      print("sukses add");
      _promos.add(Promo(
          name: "Makanan 1",
          price: 28000,
          imageUrl:
              "https://img.okezone.com/content/2019/04/26/298/2048384/5-makanan-yang-lebih-enak-beli-di-luar-daripada-masak-di-rumah-aVhG25pSQF.jpg",
          description: "heheheh",
          isFavorite: false));
      _promos.add(Promo(
          name: "Makanan 1",
          price: 28000,
          imageUrl:
              "https://img.okezone.com/content/2019/04/26/298/2048384/5-makanan-yang-lebih-enak-beli-di-luar-daripada-masak-di-rumah-aVhG25pSQF.jpg",
          description: "heheheh",
          isFavorite: false));
      _promos.add(Promo(
          name: "Makanan 1",
          price: 28000,
          imageUrl:
              "https://img.okezone.com/content/2019/04/26/298/2048384/5-makanan-yang-lebih-enak-beli-di-luar-daripada-masak-di-rumah-aVhG25pSQF.jpg",
          description: "heheheh",
          isFavorite: false));
      _promos.add(Promo(
          name: "Makanan 1",
          price: 28000,
          imageUrl:
              "https://img.okezone.com/content/2019/04/26/298/2048384/5-makanan-yang-lebih-enak-beli-di-luar-daripada-masak-di-rumah-aVhG25pSQF.jpg",
          description: "heheheh",
          isFavorite: false));
      return _promos;
    } on SocketException {
      throw FetchDataException();
    }
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
