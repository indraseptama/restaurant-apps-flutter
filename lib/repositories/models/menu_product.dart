import 'package:awesomeapp/repositories/models/product.dart';
import 'package:flutter/cupertino.dart';

class MenuProduct {
  String name;
  List<Product> products;

  MenuProduct({@required this.name, @required this.products});

  factory MenuProduct.createMenuProductFromJson(Map<String, dynamic> map) {
    List<dynamic> listProductFromJson = map["product"];
    List<Product> listProduct = [];

    for (dynamic productJson in listProductFromJson) {
      listProduct.add(Product.createProductFromJson(productJson));
    }

    return MenuProduct(name: map["name"], products: listProduct);
  }
}
