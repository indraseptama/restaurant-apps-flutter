import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Outlet {
  String name;
  String address;
  LatLng position;

  Outlet(
      {@required this.name, @required this.address, @required this.position});

  factory Outlet.createOutletFromJSON(Map<String, dynamic> object) {
    return (Outlet(
        name: object["name"],
        position: LatLng(object["lat"].toDouble(), object["lgt"].toDouble()),
        address: object["address"]));
  }
}
