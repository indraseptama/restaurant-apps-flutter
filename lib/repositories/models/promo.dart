class Promo {
  String name;
  int price;
  String description;
  String imageUrl;
  bool isFavorite;
  Promo(
      {this.name,
      this.price,
      this.description,
      this.imageUrl,
      this.isFavorite = false});
}
