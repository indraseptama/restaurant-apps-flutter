import 'package:awesomeapp/repositories/local_db/database_provider.dart';
import 'package:equatable/equatable.dart';

class Product extends Equatable {
  int id;
  String name;
  int price;
  int quantity;
  String imageUrl;

  Product({this.id, this.name, this.imageUrl, this.price, this.quantity});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_NAME: name,
      DatabaseProvider.COLUMN_IMAGE: imageUrl,
      DatabaseProvider.COLUMN_PRICE: price,
      DatabaseProvider.COLUMN_QUANTITY: quantity,
    };

    if (id != null) {
      map[DatabaseProvider.COLUMN_ID] = id;
    }

    return map;
  }

  Product.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseProvider.COLUMN_ID];
    name = map[DatabaseProvider.COLUMN_NAME];
    imageUrl = map[DatabaseProvider.COLUMN_IMAGE];
    price = map[DatabaseProvider.COLUMN_PRICE];
    quantity = map[DatabaseProvider.COLUMN_QUANTITY];
  }

  factory Product.createProductFromJson(Map<String, dynamic> map) {
    String name = map["name"];
    String imageUrl = map["imageUrl"];
    int price = map["price"];

    return Product(name: name, imageUrl: imageUrl, price: price, quantity: 1);
  }

  @override
  List<Object> get props => [id, name];
}
