import 'package:awesomeapp/repositories/models/outlet.dart';
import 'package:flutter/cupertino.dart';

class City {
  String name;
  List<Outlet> outlets;

  City({@required this.name, @required this.outlets});

  factory City.createCityFromJson(Map<String, dynamic> object) {
    List<dynamic> listOutletFromJson = object['outlets'];
    List<Outlet> listOutlet = [];
    for (dynamic outletJson in listOutletFromJson) {
      listOutlet.add(Outlet.createOutletFromJSON(outletJson));
    }
    return City(name: object["name"], outlets: listOutlet);
  }
}
